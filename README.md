# The Open University Rover Challenge Team Blog
Official Open University Rover Challenege Team Website.

## Version
* 1.0.0
* August 2018

## Dependencies

### Python
* Python 3.6.5
* Django 2.0.8
* Wagtail 2.2.2
* psycopg2 2.7.5

### Additional
* Postgres

## Dev Dependencies
* Node
* NPM
* node-scss 4.9.3
* Pure CSS Framework 1.0.0

#### Author
* James Chadwick, 2018
