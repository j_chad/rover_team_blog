from django import template
from blog.models import Header

register = template.Library()


@register.inclusion_tag('tags/headers.html', takes_context=True)
def headers(context):
    return {
        'headers': Header.objects.all(),
        'request': context['request'],
    }
